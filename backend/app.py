from flask import Flask
from flask_restplus import Api, Resource, fields
from werkzeug.contrib.fixers import ProxyFix
from flask_cors import CORS, cross_origin


app = Flask(__name__)
cors = CORS(app)
app.config['CORS_HEADERS'] = 'Content-Type'
app.wsgi_app = ProxyFix(app.wsgi_app)
api = Api(app, version='1.0', title='TodoMVC API',
    description='A simple TodoMVC API',
)

ns = api.namespace('todos', description='TODO operations')

clientes = [
    {'idade': 27, 'name': 'Pedro'},
    {'idade': 25, 'name': 'Lucas'}
]


@ns.route('/')
class Index(Resource):
    @ns.doc('list_todos')
    def get(self):
        '''List all tasks'''

        #CONSULTA DE BANCO DE DADOS
        #PROCESSAMENTO
        #ETC

        return {'dados': clientes}


@ns.route('/<int:id>')
@ns.response(404, 'Todo not found')
@ns.param('id', 'The task identifier')
class IndexId(Resource):
    '''Show a single todo item and lets you delete them'''
    @ns.doc('get_todo')
    def get(self, id):
        '''Fetch a given resource'''
        return {'data': clientes[id]}


if __name__ == '__main__':
    app.run()